from django.shortcuts import redirect, render
from lists.models import money
import operator
# Create your views here.


def home(request):
    if(request.method == 'POST' and request.POST.get(
                                    'delete', '') == 'delete'):
        id_data = request.POST['id_delete']
        money.objects.get(pk=id_data).delete()
        return redirect('/')
    if(request.method == 'POST'and request.POST.get(
                                   'button_send', '') == 'send'):
        date_text = request.POST['date']
        cost_text = float(request.POST['cost'])
        money.objects.create(
                date=date_text,
                cost=cost_text
               )
        return redirect('/')
    moneys = money.objects.all()
    cost = 0
    avg = 0
    count = 0
    maxx = 0
    minn = 999999999
    tem = 0
    text = ''
    for m in moneys:
        cost = cost+m.cost
        count = count+1
        if (maxx < m.cost):
            maxx = m.cost
        if (minn > m.cost):
            minn = m.cost
    if (count == 0):
        count = 1
    avg = cost/count
    avg = format(avg, '.2f')
    if (cost == 0):
        cost = ''
        avg = ''
        maxx = ''
        minn = ''
    if(request.method == 'POST'and request.POST.get(
      'button_send_max', '') == 'send_max'):
        n = 1
        for n in moneys:
            for m in moneys:
                if (n.cost > m.cost):
                    tem = n.cost
                    n.cost = m.cost
                    m.cost = tem
                    text = n.date
                    n.date = m.date
                    m.date = text
        return render(request, 'home.html', {
               'moneys': moneys, 'cost_money': cost, 'avg': avg,
               'maxx': maxx, 'minn': minn})
    if(request.method == 'POST'and request.POST.get(
      'button_send_min', '') == 'send_min'):
        n = 1
        for n in moneys:
            for m in moneys:
                if (n.cost < m.cost):
                    tem = m.cost
                    m.cost = n.cost
                    n.cost = tem
                    text = m.date
                    m.date = n.date
                    n.date = text
        return render(request, 'home.html', {
               'moneys': moneys, 'cost_money': cost, 'avg': avg, 'maxx': maxx,
               'minn': minn})
    return render(request, 'home.html', {
           'moneys': moneys, 'cost_money': cost, 'avg': avg, 'maxx': maxx,
           'minn': minn})
