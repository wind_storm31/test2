from django.template.loader import render_to_string
from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from lists.views import home
from lists.models import money
import pep8


class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)

    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['date'] = '17/03/2558'
        request.POST['cost'] = '100'
        request.POST['button_send'] = 'send'

        response = home(request)

        new_item = money.objects.first()
        self.assertEqual(new_item.date, '17/03/2558')
        self.assertEqual(new_item.cost, 100.0)

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home(request)

    def test_home_page_displays_all_list_items(self):
        money.objects.create(date='itemey 1')
        money.objects.create(date='itemey 2')

        request = HttpRequest()
        response = home(request)

        self.assertIn('itemey 1', response.content.decode())
        self.assertIn('itemey 2', response.content.decode())


class ItemModelTest(TestCase):

    def test_saving_and_retrieving_items(self):
        first_item = money()
        first_item.date = 'The first (ever) list item'
        first_item.save()

        second_item = money()
        second_item.date = 'Item the second'
        second_item.save()

        saved_items = money.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.date, 'The first (ever) list item')
        self.assertEqual(second_saved_item.date, 'Item the second')


class TestCodeFormat(TestCase):

    def test_functional_test(self):
        pep8style = pep8.StyleGuide(show_source=True)
        result = pep8style.check_files([
                 '/home/poo/test/superlists/functional_tests.py'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")

    def test_test(self):
        pep8style = pep8.StyleGuide(show_source=True)
        result = pep8style.check_files([
                 '/home/poo/test/superlists/lists/tests.py'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")

    def test_test(self):
        pep8style = pep8.StyleGuide(show_source=True)
        result = pep8style.check_files([
                 '/home/poo/test/superlists/lists/views.py'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")
