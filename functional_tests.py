from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_send_data(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('keep money', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('เก็บข้อมูลการหยอดกระปุก', header_text)
        inputbox_date = self.browser.find_element_by_id('date_new')
        self.assertEqual(
                inputbox_date.get_attribute('placeholder'),
                '00/00/2500'
        )
        inputbox_cost = self.browser.find_element_by_id('cost_new')
        self.assertEqual(
                inputbox_cost.get_attribute('placeholder'),
                '0.0'
        )
        inputbox_date = self.browser.find_element_by_id('date_new')
        inputbox_cost = self.browser.find_element_by_id('cost_new')
        inputbox_date.send_keys('17/03/2558')
        inputbox_cost.send_keys('100.0')
        button = self.browser.find_element_by_id('button')
        button.click()
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('td')
        inputbox_date = self.browser.find_element_by_id('date_new')
        inputbox_cost = self.browser.find_element_by_id('cost_new')
        inputbox_date.send_keys('18/03/2558')
        inputbox_cost.send_keys('400.0')
        button = self.browser.find_element_by_id('button')
        button.click()
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('td')
        self.assertIn('17/03/2558', [row.text for row in rows])
        self.assertIn('100.0', [row.text for row in rows])
        self.assertIn('18/03/2558', [row.text for row in rows])
        self.assertIn('400.0', [row.text for row in rows])
        # เชคค่าเฉลี่ย ค่า min ค่า max ค่า ผลรวม
        self.assertIn('250.00', [row.text for row in rows])
        self.assertIn('100.0', [row.text for row in rows])
        self.assertIn('400.0', [row.text for row in rows])
        self.assertIn('500.0', [row.text for row in rows])


if __name__ == '__main__':
    unittest.main(warnings='ignore')
